



# ALLTOGETHER   =\
# together_1.pdf \
# together_2.pdf \
# together_3.pdf \
# together_4.pdf \
# together_5.pdf \
# slides.pdf : slides.md human1.pdf human2.pdf ${ALLTOGETHER}; pandoc -t beamer -o $@ $<
slides.pdf : slides.md; pandoc -t beamer -o $@ $<
# human%.pdf    : human%.tikz         ; pdflatex $<
# together_%.pdf: together_%.latex    ; pdflatex $<
# 
# together_1.latex: together.latex    ; sed 's/%[1-1]//' $< > $@
# together_2.latex: together.latex    ; sed 's/%[1-2]//' $< > $@
# together_3.latex: together.latex    ; sed 's/%[1-3]//' $< > $@
# together_4.latex: together.latex    ; sed 's/%[1-4]//' $< > $@
# together_5.latex: together.latex    ; sed 's/%[1-5]//' $< > $@

# ${ALLTOGETHER_LATEX}:  together.latex; echo $1 sed 's/[1-$*]//' $< > $@

# together_5.pdf \
# ${ALLTOGETHER}: together.latex
# 	seq 5|xargs -IX sh -c

# wget -O images/enfant.jpg 'https://sitetab1.ac-reims.fr/lp-clement/-joomla-/images/stories/5-periscolaire/projets/2014_travail_enfants/Sans%20titre-0.jpg'

# wget -O images/camion.jpg 'http://www.formalourd.ca/wp-content/uploads/2012/12/CAMION-MINIER-1.jpg'
# wget -O images/sodom.jpg https://acmi.s3.amazonaws.com/media/thumbs/uploads/images/welcome_to_sodom_film_poster.jpg.1200x630_q85_crop_detail.jpg
# FROMWEB = curl -sLko $@
# 
# IMAGES +=\
# images/GlobalPeakOil.png
# images/GlobalPeakOil.png:
# 	$(FROMWEB) https://upload.wikimedia.org/wikipedia/commons/f/fe/GlobalPeakOil.png
# 
# wget -O 800kg.webp https://cdn.radiofrance.fr/s3/cruiser-production/2019/06/2e0fb26d-d6cc-49f4-aa2b-aba528bff2f8/838_0601318878407-web-tete.webp
# 
# images/logo/%.pdf: images/logo/%.svg; rsvg-convert -f pdf -o $@ $<
# 
# IMAGES +=\
# images/GlobalPeakOil.png
# images/GlobalPeakOil.png:
# 	$(FROMWEB) https://upload.wikimedia.org/wikipedia/commons/f/fe/GlobalPeakOil.png
# 
# IMAGES +=\
# images/world3.jpg
# images/world3.jpg:
# 	$(FROMWEB) http://ekladata.com/uw-TF8hgRQ9FA8pBnGPN8VEXSso@600x486.jpg
# 
# IMAGES +=\
# images/energy_sources.jpg
# images/energy_sources.jpg:
# 	$(FROMWEB) https://jancovici.com/wp-content/uploads/2016/04/energie_graph1.jpg
# 
# IMAGES +=\
# images/disaster.jpg
# images/disaster.jpg:
# 	$(FROMWEB) https://s3.r29static.com/bin/entry/b3e/430x516,80/1621855/image.jpg
# 
# IMAGES +=\
# images/ge.png
# images/ge.png:
# 	$(FROMWEB) http://www.igbp.net/images/18.950c2fa1495db7081eba4/1421318144335/great_accel-12graph-SE.png
# 
# IMAGES +=\
# images/maunaloa.jpg
# images/maunaloa.jpg:
# 	$(FROMWEB) https://cdn.futura-sciences.com/buildsv6/images/mediumoriginal/5/f/9/5f9fb17081_105808_taux-dioxyde-carbone-02.jpg
# 
# IMAGES +=\
# images/415ppm.jpg
# images/415ppm.jpg:
# 	$(FROMWEB) 'https://pbs.twimg.com/media/D5xzAzBXoAEWcU3?format=jpg'
# 
# IMAGES +=\
# images/oms.pdf
# images/oms.pdf:
# 	curl -sLko tmp.oms.svg https://apps.who.int/iris/themes/WHO//images/fr/who_logo.svg
# 	rsvg-convert -f pdf -o $@  tmp.oms.svg
# 
# IMAGES +=\
# images/industry.jpg
# images/industry.jpg:
# 	$(FROMWEB) https://resize.hswstatic.com/w_907/gif/anthropocene.jpg
# 
# IMAGES +=\
# images/biodiversite.jpg
# images/biodiversite.jpg:
# 	$(FROMWEB) http://www.humandee.org/IMG/jpg/MEA._Biodiversite_FR.jpg
# 
# IMAGES +=\
# images/drunk.jpg
# images/drunk.jpg:
# 	$(FROMWEB) https://fr.homenal.com/wp-content/uploads/2018/05/hilarante04.jpg
# 
# IMAGES +=\
# images/logo.pdf
# images/logo.pdf: ../rc/Logo-JRES2019/Logo-Couleur/Logo-JRES2019-Couleur.ai
# 	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$@ $<
# 
# IMAGES +=\
# images/jobs.jpg
# images/jobs.jpg:
# 	$(FROMWEB) http://jovanabanovic.com/wp-content/uploads/2017/12/Steve-Jobs.jpg
# 
# IMAGES +=\
# together.pdf
# together.pdf: together.latex
# 	pdflatex $<
# 
# # TODO: permission contact, humandee.org
# # http://www.indigne-du-canape.com/la-6e-extinction-de-masse-est-en-cours-et-aucun-politicien-ny-changera-rien/homme-scie-branche-assis/
# IMAGES += images/welcome-to-anthropocene.jpg
#           images/welcome-to-anthropocene.jpg:
# 	$(FROMWEB) http://www.indigne-du-canape.com/wp-content/uploads/2016/12/homme-scie-branche-assis.jpg
# 
# slides.md: $(IMAGES)
# clean:; rm $(IMAGES) slides.pdf
# deploy:; scp slides.pdf aude:www/pub/jres2019-slides.pdf
