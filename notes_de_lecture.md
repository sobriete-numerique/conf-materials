# keynote?

[keynotes pierre-yves longaretti](https://www.liglab.fr/fr/evenements/keynote-speeches/pierre-yves-longaretti-global-environmental-collapse-risks-is-digital)

        01:48 Limit of the growth, 2004 ?????????? je dirais 1972 moi.
        04:34 On a pas de problème de surpopulation mais de modèle de surconsommation

              Pour faire vite: le problème c'est le mode de vie occidental,
              pas la surpopulation

            * une étude récente montre que même sans faire appel à la
              collapsologie, la transition démographique aurait permis de
              stabiliser la population vers 2050
            * plusieurs études à croiser pour montrer que
              * c'est l'économie de marché qui est l'origine du problème
                * production et acheminement organisé autour de la seule
                  rentabilité économique
                * stimulation de la demande par l'explosion d'offres de moins
                  en moins durables (fragilité, obsolescence programmée)
              * cette économie profite à une frange très réduite de la
                population

        26:20: good point!
            * unlimited desires
            * competition
                => stress desire strategy

        29:00 society complexity ? 

            * étant ingénieur, ca me parle mais je ne crois pas que ce soit
              le principal probleme! cf. point précédent

            * je crois au principe de dilbert: décomplexifier, c'est admettre
              l'inutilité des élites et de la compétition

        34:00 super intéressant !!! si on remplace complexity par rentabilité

        40:00 les memes graphiques que le shift

            * j'aimerais mettre la main sur
                * les sources exactes
                * les données brutes
            * meme question pour la mixité énergétique

        41:19 bon résumé des impacts écologiques

        43:40
            * illusion of demat est le fruit d'un discours
            * indirect effects et hidden costs: audition senat giraud!!

        48:29 *super intéressant*
            Transition numérique vs ecologique 😍
            Colapse is on its way: imagine solutions w or w/o numerics

TODO: réencoder la video juste avec les slides pour voir combien on gagne

# Colloque numérique

[direct](https://app.livestorm.co/dge-ministere-de-l-economie-et-des-finances/colloque-numerique-et-environnement).

* pas grand chose à apprendre mais une belle demonstration de "croissance verte".
* rappeller qu'il faut être normatif: "responsable" ne veut rien dire.
* ils parlent de "boites noires" comme une fatalité!!! :'( promotion des OS libres urgente
* donner des alternatives aux citoyens: trouver un nom pour les possibles ergonomiques
* revoir la conférence du champion pour reprendre les éléments de langage dont
  qui coulent dans les ministères ???
* "on ne sait pas" alors que des premières publics datent de 90: ignorance au plus haut
  niveau? enfumage?
* du big data pour produire du bon sens et du cooling ??? y'a vraiment des directeurs
  de labo qui ont faim !!
* ecoinfo sera toujours isolé parce que nous ne pourrons/devrons pas tenir le discours
  de la "greentech", mélange de greenwashing et de manque de veille.
* pas de compétition possible sur le search ou cloud??? OSEF: 2 modèles à détruire

... bon il me fatigue le champion et faut que j'y aille ...

Réinventer le numérique ->  pour quelle société? -> projet politique plus que technique
abstraction en temps qu'informaticien -> -> laissons les ingénieurs optimiser leurs CMS

# Shift project "déployer la sobriété numérique"

* dans 5 ans la flotte mondiale de voiture mais c'est pas très amish tout ça ...
* les antis disent stabilité: c'est pas assez.
* "Intempérence naturelle" passe tellement mieux que "égoisme criminel"
* "comment faire comprendre aux utilisateurs?"
    ben en leur expliquant :)
* Université de louvain a contacter pour la partie electronique?

terminologie: "démarche operationelle de sobriété numérique" semble établie

JMJ: l’essentiel du trafic est de la vidéo en ligne, essentiellement
récréative. Cela a été détaillé dans le rapport
https://theshiftproject.org/article/climat-insoutenable-usage-video/

#### Pertinence énergétique

Smart cities: tenter de responsabiliser les acteurs

Yes, it's possible to try. Our model "STERM" might help a bit. But we have not done it.

Direction générale pas les impacts suffisants

apres ... j'ai du m'endormir ...

