# Marc Chantreux

    IGE à l'unistra │ mc(at)unistra.fr
    MAD à Renater   │ marc.chantreux(at)renater.fr
    Réseaux sociaux │ eiro(at)phear.org

    # Mesure de l'existant
    # ecodiag voltmètre
    # recherche marginale de marges
    # XML|ENSIMAG
    # optimisations
    # mesure initiale
# ecodiag
# les vieux dossiers! plan9/tty(audrey tang,...)
# cultures tech
    # prouve le! bench, mesures
    # pi as dev machine
    # text first, pay next
    #
    # decentralisation
# 50 nuances de green
    # Business as Usual: on continue a bosser sur la réalité augmentée, ...
    # GreenWashing: la même en trovant des excuses|arguements de vente
    # La croissance verte (du Big Data pour notre agriculture ...)
    # Eco conception et responsabilisation
    # Visions plus radicales (informatique post-personelle)
# faire un travail de veille, pas de coordination
    # travaux présents et redondants
    # initier et passer le relais, garder le lien
    # lien entre un besoin et une compétences
    # accompagner le blues du chercheur ?
# Education/Sensibilisation
    # faire des confs grand public jusqu'a ce que d'autres s'en emparent
    # pas s'adresser aux étudiants mais participer à la construction de programmes.
    # les batiments
# évenements a venir et structurants
# disparition des glaces
# si la fabrique du concentement a marché dans un sens pour forger un homo-economicus consommateur de docile, ne peut-elle marcher dans l'autre?
# economiquement:
https://fr.wikipedia.org/wiki/Ressources_et_consommation_%C3%A9nerg%C3%A9tiques_mondiales

  * consommation d'énergie en hausse
  * aucune des "avancées" technique ou politique n'est observable significativement sur les graphiques
  * la transition énergétique n'existe pas

# la

  * le marché n'anticipe pas la rupture de stock
      => disponibilité et illusion d'abondance
  * les chocs pétroliers conjoncturel
    => petites voitures
  * les chocs pétroliers structurel
    => ????
  * hydrocarbures non conventionnels
    * pic entre 10 et 60 ans

# c'est politique?
    # personel technique du public
        obtenir un niveau d'information suffisant pour
        * initier de bonnes pratiques
        * participer (meme indirectement)
        tenter d'initier des bonnes pratiques
        tenter d'influer sur la stratégie de l'organisation
    # citoyen * vous êtes des citoyens: s'informer, voter, eco-gestes,
    # humain
    # terrien
    vous êtes concernés
    # gestionnaire de parc
    # personel d'entretient: avec quoi nettoyez-vous vos sols?
    # personnel administratif? *vous* êtes accusés d'être un frein à la sobriété!
    # faites savoir a vos hiérachies que moyenant accompagnement, vous êtes prets
Attractivité!
législation va devenir de plus en plus contraignante

Contrat objectif performances.
Inria Sophie Quinton  que malgré le manque de visibilité sur la rerappellait qu'on est déjà
EPFL ETHZ, Princetown, Berlin. portugal.
si vous allez dans les milieux minimalistes (suckless, bitreich) des ingénieurs! 
Attractivité + 
Efficacité vs Efficience
Exemplarité
Contrainte par la loi sur les mesures

# Renater

* principale activité: fournisseur d'accès Internet et de services numériques pour l'ESR


* inscrire l'éco-responsabilité dans sa stratégie
* sensibiliser aux problèmes environnementaux et aux impacts du numérique
* ACV (Analyse du Cycle de Vie) avec Ecoinfo
    * un segment du backbone
    * les services de messagerie et de visio
* initier un groupe de travail environnement

# groupe de travail environnement

Devenir une cellule ayant pour mission la réduction de l'impact
environnemental des services et réseaux de Renater

* sensibilisation/informations
* production d'indicateurs de pilotage
  * consommation, ACV, ...
  * mise en perspective
    * conformité avec la législation
    * recommandations externes, moyennes et optimaux actuels
* recommandations pour la pédagogie et les usages
* veille et activation de mesures et d'expérimentations

# Extraction des ressources

    ~ 800Kg de terre pour un PC il y a quelques années
    on trouverait probablement plus hui parce que
    pic d'Hubbert (~1940) (presque gaussien).

    le rendement d'une extraction
    * augmente pendant la phase d'industrialisation
    * diminue pendant la raréfaction de la ressource

    La raréfaction c'est aussi
    * plus de ressources à mettre en œuvre

    * premières extractions artisanales
    * industrialisation
    * pic
    * chute du 

# probablement plus de 800Kg

    * valable pour toute extraction de ressource finie

# rapport meadows


    * le prix reflète la disponibilité, pas l'abondance


    Décharge/Incinération/Recyclage

# Hydrocarbures fossiles (pétrole, charbon, gaz)


    Extraction (800Kg de terre pour un PC)

# Réduire la pollution technologique

(voitures, avions, super-camions)

C'est avant tout éviter que ces objets vivent

* partager
* faire veillir
* réparer

⇒ organiser un sytème de réparation et d'occasion


C'est aussi



# crises environnementales

    zones inhabitables (principalement sur l'équateur)
    crises humanitaires et migratoires
    slide OMS

# Prélude

Le présent diaporama disponible dans un format affichable sur des
teletypewriter (1962?) et indépendant du flux video.

# Pour la collaboration

    git (efficience et richesse fonctionelle)

# Pour la diffusion

la partie audio:

    * pourrait utiliser un système de broadcast avec des terminaux
      extrèmement simples (téléphone filaire)

la partie vidéo:

  * pèse ~ 10kb (bande passante et archivage faible)
  * temps de calcul de compression|décompression presque inexistant ou
    négligeable (|gzip).
  * nécessite des dispositifs matériels et logiciels relativement simples
    à produire et à maintenir (tant pour la conception que pour l'usage).
    (comparer le code de st à celui d'un navigateur de firefox ou vlc ...)

# pour la rediffusion

  * table de correspondance temps/évenement clavier

# pour la traduction

  * texte simple
  * table de correspondance temps/évenement clavier

# Pourquoi pas? manque d'outils "grand public"

La promotion de ce genre d'outils n'est pas à l'ordre du jour
alors que:

* nous disposons déjà de toutes les technologies (open source)
* leurs intégration dans une interface grand public est réalisable
  simplement.
* le travail a déjà été partiellement réalisé par des communautés
  de technophiles dit "minimalistes" (bitreich conference 2020)

# Pourquoi?

* le marché du numérique capitalise sur des technos et usages
  déjà produits et adoptés dans une compétition perpétuelle visant à

    * produire un désir chez l'utilisateur (créer une demande)
    * y répondre par des dispositifs disproportionnés (créer une offre)

    nous avons tous (personnes, entreprises, labos, ...) une expertise
    qui nous permet de nous inserer dans ce système qui nous récompense par la
    possibilité de de céder aux désirs que d'autres ont créé. sortir
    du système c'est renoncer/repenser notre capacité à nous inserer dans le
    marché à titre individuel et collectif.

    * les psys nous expliquent que cette boucle est infinie
    * de nombreux autres spécialistes nous expliquent que casser
      cette boucle est devenu une question de survie

* la nécessité d'une décroissance numérique aussi forte et questionnée
  pour ma part je dirais que
  * chaque octet et cycle CPU facilement économisable permet de conserver
  * une préparation suffisante sera intéressante lorsque la rareté de
    l'énergie et des matières premières va commencer
  * en cas de quasi-pénurie, si une informatique est encore utile, 

    nécessaire et possible, je ne crois pas que ce sera via des navigateurs
    web sous windows.


    de l'informatique survit dans ce contexte

    à peser sur les prix

    
  
  les prix des matières premières et de l'énergie
  reflèteront


  * en l'occurrence, les freins ne sont que culturels








* chez de nom



* une éco conception fortement décroissante est
    * encore con la nécessité


    * mal comprise
    *



* la nécessité et





* notre condition d'homo-economicus




      boucle doit pre
      * les ressources, elles, sont finies
      * les 





* les utilisateurs

# Questions qui se posent
* l'économie réelle pour l'écologie?
    * pas encore de mesures (vous pouvez commencer ce travail)



Nous ne *voulons pas* promouvoir ce genre de solutions 




dans des interfaces manipulables par 




dans un outils accessible au grand public serait


Les logiciels qui dominent le marché de la visio ne permettent pas ce genre
d'usage.


Produire un client simple pour ce genre d'usage

  * impose de disposer des sources de données et non de graphiques
  * pas d'images mais des pointeurs (de toute façon je n'avais pas les droits)






  * moins de code c'est
    * réduire le temps de développement et de maintenance
    * réduire les surfaces d'attaque
    * réduire 

#


* impossible d'utiliser mon format parce que

  * il n'existe aucun dispositif preconfiguré sur les postes des utilisateurs
  * l'immense majorité d'entre eux ignorent ce qu'est un terminal et en ont
    peur. (c'est culturel)
    * c'est construit
    culturellement
    *



      * plus "faciles" à produire et à maintenir
  * temps de calcul de compression|décompression presque inexistant (|gzip)



# Questions

* quelle est l'économie énergétique réalisée de bout en bout?

    ⇒ la question doit prend en compte l'extraction de

*
* je ne suis pas psychologue mais les 3 dernières années me font dire



    * quelle est l'économie énergétique réalisée de bout en bout?



* est-ce vraiment nécessaire?
* 


# le changement climatique

    * réchauffement du climat
    * intensification des phénomènes météo

# 6ème extinction de masse

disparition de la biodiversité

    * protection naturelle contre les risques biologiques
    * 



    * Réchauffement climatique
    * Raréfications des ressources


    (au moins) 50 ans de science sur le sujet

    informatique, mathématiques
    Physique(s), Sciences et vie de la terre,
    Histoire, Économie, Psychologie,
    Géologie, Géographie, Géopolitique,
    Climatologie, Météorologie, Mathématiques
    ...

# à défaut d'être sûr

    être honnête, transparent, humble
    * ouvert à d'autres visions
    * conscient de sa partialité

    s'en remettre aux résultats de recherche
    * prédictions correctes
    * dynamique et ordres de grandeurs

# 




# sobriété et convivialité numérique

# Définitions

    Numérique    : Objets sociétaux
    Informatique : Objets techniques

# Convivialité

    Hispanisme utilisé par Ivan Illich dans La convivialité (1973) pour
    qualifier à la fois des outils dont la fonction est déterminée par celui
    qui les manie plutôt que par celui qui les conçoit, et un type de société
    post-industrielle caractérisé par ces outils, l'autonomie et l'interdépendance.

    -- Wikipedia

# Sobriété

    L'application d'une volonté individuelle et collective
    de mobiliser la quantité la plus faible possible de
    certains moyens pour la réalisation d'un but
    ou d'une nécessité.

    ⇒ identifier les objets de la sobriété
      (les moyens et les buts personels et collectifs)
    ⇒ recontextualiser ses objets pour en questionner
      la pertinence et la faisabilité

# L'informatique moderne a toujours été sobre

# les débuts de l'informatique moderne

    - moyens techniques
    + moyens economiques
        + ressources humaines
        + culture scientifique
        + inovation ouverte
    ⇒ - buts
    ⇒ - audience (défense, recherche, ...)

# micro-informatique des hobbyistes

    - presque tout
        + ressources humaines
        + culture techi
        + inovation ouverte
    - audience réduite (technophiles)


    débuts de l'informatique
    ⇒ - moyens techniques
    ⇒ + moyens economiques
        ⇒ + ressources humaines
        ⇒ culture scientifique et inovation ouverte
    ⇒ - buts
        ⇒ audience réduite (défense, recherche, ...)









# les débuts

    débuts de l'informatique
    ⇒ + moyens techniques
    ⇒ + moyens economiques
        ⇒ + ressources humaines
        ⇒ + culture scientifique
    ⇒ - buts

# L'informatique moderne a toujours été sobre



# mesurer

    débuts de l'informatique et de la micro
    ⇒ + ressources humaines
    ⇒ + ressources humaines
    ⇒ buts limités

    débuts de l'informatique et de la micro
    ⇒ + ressources humaines
    ⇒ buts limités


* 





      (les moyens et les buts personels et collectifs)
    ⇒ recontextualiser ses objets pour en questionner
      la pertinence et la faisabilité




# problèmes environementaux


# Informatique

* politique: accelerateurs



# nous n'avons pas le temps

* pas assez de mesures: le champ est nouveau
* pas assez d'organisation: redondance, éparpillement, immobilisme
* a défaut d'être sur, il faut être honnête et transparent.
  ce a quoi je m'essaye hui.

audrey tang / ministre du numérique de taiwan
    -> alternative a reddit is "terminal based"
    -> ils ont misé sur la culture, pas sur le service

* coordination/veille nationale objective et consultative
* accueillir les chercheurs qui "craquent"?
* assainir le fonctionnement de la recherche:
    * pas besoin de matos: on a tous des pi
    * besoin d'humains, de moyens pour se coordonner

la coopération



les brevets!

* préparer la sobriété
  accompagner

  quelle sobriété ???

réduire la facture tout de suite mais aussi préparer à
des usages dans des moyens restreints

* des évenements possibles, impactants et structurants

    l'éffondrement(des sociétés industrialisée)
    une transition énergétique
    une bonne idée chez spaceX

* pour les anticiper et/ou en comprendre l'impact,
  il faut invoquer

  la physique, la biologie, la chimie,
  l'informatique,
  la philosophie politique, l'histoire,
  la démographie, l'économie, la géopolitique

* pour savoir quel numérique, il faut savoir quelle société
* nous manquons de temps, il faut experimenter, créer de l'interdisciplinarité
* nous ne pouvons pas explorer toutes les pistes

    * etre honnête et transparent à defaut d'être sur (opendata,release earlier...)
    * ne pas exclure l'hypothèse que nous ayons tord
    * laisser chacun travailler sur

* nous ne pouvons pas explorer toutes les pistes

50 nuances de green
* je ne suis pas futurologue
* ma vision est bien plus "optimiste" que celle des cyberchampions:
  * une transition numérique qui s'inscrit
  * une refonte sociétale
  * je pense que nous pouvons repenser

Aux utilisateurs/ Supports / Usages numériques

    * comprendre que leurs désirs ne seront jamais satisfaits
    * comprendre les objectifs de l'informatique que nous allons leur proposer
    * comprendre les indicateurs pour repenser leurs consommation
    * miser sur la formation, la délégation et la généralisation plus que
      sur la capacité des DSI


Aux gestionnaires de parc
    * ralentir la rotation matérielle: allonger la durée de vie des parcs
    * comprendre et publier les ACV aux décideurs (pilotage, contraintes)
      et aux utilisateurs.
    *
      et aux utilisateurs.

Aux décideurs
    de comprendre que tout ca aura un cout
    se fera contre la volonté des éxigents au debut
    de faire la pédagogie

Aux décideurs
    de comprendre que tout ca aura un cout
    se fera contre la volonté des éxigents au debut
    de faire la pédagogie




* nous manquons de données
    * sur la 

<html

(zig et crystal)




